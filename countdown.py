from datetime import datetime, timedelta
from typing import Callable

import rich.repr
from rich.align import Align
from rich.console import RenderResult

from figlet_text import FigletText

from textual.widget import Widget
from textual.message import Message
from textual.reactive import Reactive
from textual._types import MessageTarget

from asyncio import Queue


@rich.repr.auto
class CountdownEnded(Message, bubble=True):
    def __init__(self, sender: MessageTarget) -> None:
        super().__init__(sender)


@rich.repr.auto
class CooldownEnded(Message, bubble=True):
    def __init__(self, sender: MessageTarget) -> None:
        super().__init__(sender)


class Countdown(Widget):
    duration = Reactive(0)
    cooldown = Reactive(0)
    stopping = Reactive(False)
    text = Reactive("")
    nextTick = Reactive(datetime.now())
    paused = Reactive(False)
    pauseRemaining = Reactive(timedelta(seconds=0))

    def __init__(
        self,
        duration: int,
        cooldown: int = 0,
        finalcont: str = None,
        alignfn: Callable[[str], Align] = lambda x: Align.center(
            FigletText(x), vertical="middle"
        ),
    ):
        super().__init__()
        self.finalcont = finalcont
        self.alignfn = alignfn
        self.timer = None

    async def restart(self, duration: int, cooldown: int = 0) -> None:
        self.stopping = cooldown == 0 and duration == 0
        self.duration = duration
        self.cooldown = cooldown
        self.nextTick = datetime.now() + timedelta(seconds=1)
        for _ in range(self._message_queue.qsize()):
            self._message_queue.get_nowait()
            self._message_queue.task_done()

    async def stop(self) -> None:
        await self.restart(0, 0)

    async def pause(self) -> None:
        if self.paused:
            self.nextTick = datetime.now() + self.pauseRemaining
            self.paused = False
        else:
            self.pauseRemainig = self.nextTick - datetime.now()
            self.paused = True

    def on_mount(self) -> None:
        self.timer = self.set_interval(0.1, self.decrease)

    async def watch_duration(self, duration: int) -> None:
        if duration <= 0:
            if self.finalcont is not None:
                self.text = self.finalcont
            if not self.stopping:
                await self.emit(CountdownEnded(self))
        else:
            if self.cooldown > 0 and self.finalcont is not None:
                self.text = self.finalcont
            else:
                self.text = str(duration)

    async def watch_cooldown(self, cooldown: int) -> None:
        if cooldown <= 0:
            if not self.stopping:
                await self.emit(CooldownEnded(self))
            await self.watch_duration(self.duration)

    def decrease(self) -> None:
        if not self.paused and datetime.now() > self.nextTick:
            if self.cooldown > 0:
                self.nextTick += timedelta(seconds=1)
                self.cooldown -= 1
            elif self.duration > 0:
                self.nextTick += timedelta(seconds=1)
                self.duration -= 1

    def render(self) -> RenderResult:
        return self.alignfn(self.text)
