from ruamel import yaml
import random


class parser_base(yaml.YAMLObject):
    # default constructor, this is needed to actually trigger python object
    # construction, otherwise pyyaml/ruamel.yaml will load a dict without
    # running constructor code
    @classmethod
    def constructor(cls, loader, node):
        values = {}
        for (k, v) in node.value:
            key = loader.construct_scalar(k)
            if isinstance(v, yaml.SequenceNode):
                values[key] = loader.construct_sequence(v)
            elif isinstance(v, yaml.MappingNode):
                values[key] = loader.construct_mapping(v)
            else:
                values[key] = loader.construct_object(v)
        return cls(**values)


class Esercizio(parser_base):
    def __init__(self,
                 nome: str,
                 descrizione: str,
                 direzioni: [str] = None,
                 lr: bool = False,
                 ud: bool = False,
                 img: str = None,
                 autoplay: bool = None):
        self.nome = nome
        self.desc = descrizione
        if direzioni is None:
            self.direzioni = []
        elif type(direzioni) is list:
            self.direzioni = direzioni[:]
        elif type(direzioni) is str:
            self.direzioni = direzioni.split(",")
        else:
            raise TypeError(f"direzioni '{direzioni}' ({type(direzioni)}) is not valid")

        if lr:
            self.direzioni.extend(["l", "r"])
        if ud:
            self.direzioni.extend(["u", "d"])

        self.img = img
        self.autoplay = autoplay


class Gruppo(parser_base):
    def __init__(self,
                 nome: str,
                 riposo: str,
                 esercizi: [Esercizio],
                 img: str = None,
                 autoplay: bool = True):
        self.nome = nome
        self.riposo = riposo
        self.esercizi = esercizi[:]
        self.img = img
        self.autoplay = autoplay
        for e in self.esercizi:
            setattr(e, "gruppo", self)


class Programma:
    def __init__(self, pfile: str):
        loader = yaml.SafeLoader
        loader.add_constructor(u'!esercizio', Esercizio.constructor)
        loader.add_constructor(u'!gruppo', Gruppo.constructor)

        with open(pfile, 'r') as f:
            self.gruppi = yaml.load(f.read(), Loader=loader)

        for g in self.gruppi:
            for e in g.esercizi:
                if e.autoplay is None:
                    e.autoplay = g.autoplay

        self.gruppo = 0
        self.esercizio = 0

    def random(self) -> Esercizio:
        return random.choice(random.choice(self.gruppi).esercizi)

    def reset(self) -> None:
        self.gruppo = 0
        self.esercizio = 0

    def prossimo(self) -> Esercizio:
        if self.gruppo >= len(self.gruppi):
            return None
        e = self.gruppi[self.gruppo].esercizi[self.esercizio]
        self.esercizio += 1
        if self.esercizio >= len(self.gruppi[self.gruppo].esercizi):
            self.esercizio = 0
            self.gruppo += 1
        return e
