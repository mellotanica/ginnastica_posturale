#!/usr/bin/env python3

from textual.app import App
from player import Player

from programma import Programma

import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "esercizi", help="file yaml contenente la lista degli esercizi da svolgere"
)
parser.add_argument(
    "-r",
    "--ripetizioni",
    type=int,
    default=10,
    help="numero di ripetizioni per ogni esercizio",
)
parser.add_argument(
    "-d", "--durata", type=int, default=5, help="durata di ogni singola ripetizione"
)
parser.add_argument(
    "-R", "--riposo", type=int, default=3, help="durata del riposo tra ogni ripetizione"
)
parser.add_argument(
    "-a",
    "--autoplay",
    action="store_true",
    help="passa in automatico all'esercizio successivo una volta termianto il corrente",
)
parser.add_argument(
    "--ciclo",
    action="store_true",
    help="ricomincia da capo quando gli esercizi sono finiti",
)
parser.add_argument(
    "--procedi",
    default="n",
    help="pulsante per avviare l'esercizio/procedere alla prossima ripetizione",
)
parser.add_argument("--prossimo", default="e", help="passa al prossimo esercizio")
parser.add_argument("--pausa", default="p", help="metti in pausa l'esercizio corrente")

args = parser.parse_args()

p = Programma(args.esercizi)

import sys


class Posturale(App):
    async def on_load(self, event):
        self.player = Player()
        await self.bind("q", "quit")
        await self.bind(args.prossimo, "loadesercizio")
        await self.bind(args.procedi, "procedi")
        await self.bind(args.pausa, "pausa")

    async def action_loadesercizio(self) -> None:
        e = p.prossimo()
        if e is None:
            if args.ciclo:
                p.reset()
                e = p.prossimo()
            else:
                sys.exit(0)
        await self.player.cambia_esercizio(
            e, args.ripetizioni, args.durata, args.riposo
        )

    async def action_pausa(self) -> None:
        await self.player.sidebar.countdown.pause()

    async def action_procedi(self) -> None:
        await self.player.action_procedi()

    async def handle_esercizio_finito(self) -> None:
        if args.autoplay:
            await self.action_loadesercizio()

    async def on_mount(self):
        await self.view.dock(self.player)
        await self.action_loadesercizio()


Posturale.run()
