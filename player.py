from programma import Esercizio
from countdown import Countdown

from textual.widgets import Static
from textual.views import DockView
from textual.reactive import Reactive
from textual.message import Message
from textual._types import MessageTarget

from rich.console import RenderableType
from rich.panel import Panel
from rich.layout import Layout
from rich.align import Align
from rich.rule import Rule
from rich import box

from figlet_text import FigletText
from image2ascii import Image

import rich.repr

import os

os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "hide"
import pygame


@rich.repr.auto
class EsercizioFinito(Message, bubble=True):
    def __init__(self, sender: MessageTarget, esercizio: Esercizio) -> None:
        super().__init__(sender)
        self.esercizio = esercizio


@rich.repr.auto
class RipetizioneFinita(Message, bubble=True):
    def __init__(self, sender: MessageTarget) -> None:
        super().__init__(sender)


class Box(Static):
    def render(self) -> RenderableType:
        return Panel(super().render(), box=box.ROUNDED)


class Sidebar(DockView):
    lato = Reactive(None)
    ciclo = Reactive("")

    def __init__(self, player, snd_start="service-login", snd_end="service-logout"):
        super().__init__()
        pygame.mixer.init()

        self.description = Box("")
        self.countdown = Countdown(0, finalcont="riposo")
        self.ripetizioni = Box("")
        self.direzione = Box("")
        self.player = player

        self.start_snd = pygame.mixer.Sound("res/start.wav")
        self.rest_snd = pygame.mixer.Sound("res/rest.wav")
        self.end_snd = pygame.mixer.Sound("res/end.wav")

    async def on_mount(self):
        await self.dock(
            self.countdown, self.direzione, self.ripetizioni, edge="bottom", size=10
        )
        await self.dock(self.description, edge="bottom")

    async def handle_countdown_ended(self):
        if self.player.started:
            await self.player.load_gruppo_img()
            rep = self.ciclo.split("/")
            if rep[0] == rep[1]:
                if self.end_snd is not None:
                    self.end_snd.play()
            else:
                if self.rest_snd is not None:
                    self.rest_snd.play()
            await self.emit(RipetizioneFinita(self))

    async def handle_cooldown_ended(self):
        if self.player.started:
            await self.player.load_esercizio_img()
            if self.start_snd is not None:
                self.start_snd.play()

    async def watch_ciclo(self, ciclo: str):
        await self.ripetizioni.update(
            Align(FigletText(ciclo), align="center", vertical="middle")
        )

    async def watch_lato(self, lato: str):
        d = ""
        if lato is not None:
            if lato == "u":
                d = """  /\\
 /  \\
/____\\
  ||
  ||
  ||"""
            elif lato == "d":
                d = """  ||
  ||
__||__
\\    /
 \\  /
  \\/"""
            elif lato == "l":
                d = """  /|
 / |
/  |------
\\  |------
 \\ |
  \\|"""
            elif lato == "r":
                d = """      |\\
      | \\
------|  \\
------|  /
      | /
      |/"""
            else:
                d = FigletText(lato)
        await self.direzione.update(Align(d, align="center", vertical="middle"))


class Player(DockView):
    esercizio = Reactive(None)
    ripetizioni = Reactive(0)
    autoplay = Reactive(None)
    started = Reactive(False)

    def __init__(self, snd_start="device-added", snd_end="device-removed"):
        super().__init__()
        self.img = Box("")
        self.title = Box("")
        self.sidebar = Sidebar(self, snd_start, snd_end)

    def formatta_titolo(self, esercizio: Esercizio) -> RenderableType:
        layout = Layout()
        layout.split_row(
            Layout(
                Align(
                    FigletText(esercizio.gruppo.nome), align="left", vertical="middle"
                )
            ),
            Layout(
                Align(FigletText(esercizio.nome), align="center", vertical="middle"),
                ratio=3,
            ),
            Layout(Align("")),
        )
        return layout

    def formatta_descrizione(self, esercizio: Esercizio) -> RenderableType:
        layout = Layout()
        layout.split_column(
            Align(""),
            esercizio.desc,
            Rule("Posizione di riposo", style="rule.box"),
            esercizio.gruppo.riposo,
        )
        return layout

    async def cambia_esercizio(
        self,
        esercizio: Esercizio,
        ripetizioni: int,
        durata: int,
        cooldown: int,
        autoplay: bool = None,
    ) -> None:
        self.started = False
        self.esercizio = esercizio
        self.durata = durata
        self.cooldown = cooldown

        await self.sidebar.countdown.stop()
        self.ripetizioni = 1
        self.ripetizioni_tot = max(len(esercizio.direzioni), 1) * ripetizioni
        if autoplay is None:
            self.autoplay = esercizio.autoplay
        else:
            self.autoplay = autoplay

        await self.load_esercizio_img()

        await self.title.update(self.formatta_titolo(esercizio))
        await self.sidebar.description.update(self.formatta_descrizione(esercizio))

        if self.autoplay:
            await self.action_procedi()

    async def handle_ripetizione_finita(self):
        if self.started and self.autoplay is not None and self.autoplay:
            await self.action_procedi()

    async def action_procedi(self):
        if not self.started:
            await self.load_esercizio_img()
            await self.sidebar.countdown.restart(self.durata, self.cooldown)
            self.started = True
        else:
            if self.ripetizioni < self.ripetizioni_tot:
                self.ripetizioni += 1
            else:
                await self.load_gruppo_img()
                await self.sidebar.countdown.stop()
                self.sidebar.lato = None
                await self.emit(EsercizioFinito(self, self.esercizio))

    async def watch_ripetizioni(self, ripetizioni: int) -> None:
        self.sidebar.ciclo = f"{self.ripetizioni}/{self.ripetizioni_tot}"
        if ripetizioni <= self.ripetizioni_tot:
            if len(self.esercizio.direzioni) > 0:
                self.sidebar.lato = self.esercizio.direzioni[
                    self.ripetizioni % len(self.esercizio.direzioni)
                ]
            else:
                self.sidebar.lato = None
            if self.started:
                await self.sidebar.countdown.restart(self.durata, self.cooldown)

    async def load_gruppo_img(self):
        if self.esercizio and self.esercizio.gruppo.img is not None:
            await self.img.update(Image(self.esercizio.gruppo.img))
        else:
            await self.img.update("")

    async def load_esercizio_img(self):
        if self.esercizio and self.esercizio.img is not None:
            await self.img.update(Image(self.esercizio.img))
        else:
            await self.img.update("")

    async def on_mount(self):
        await self.dock(self.title, edge="top", size=5)
        await self.dock(self.sidebar, edge="right", size=60)
        await self.dock(self.img, edge="right")
