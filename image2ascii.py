from rich.console import Console, ConsoleOptions, RenderResult
from rich.align import Align
from rich.text import Text

import PIL.ImageOps
import PIL.Image

char_pixel_ratio = 0.5
chars = " .,:;i1tfLCG08@"
precision = int(255 * 3 / (len(chars)-1))

class Image:
    def __init__(self, path: str, center: bool = True, stretch: bool = False):
        super().__init__()
        self.path = path
        self.stretch = stretch
        self.center = center
        self.img = PIL.Image.open(self.path).convert('RGBA')
        self.width, self.height = self.img.size
        self.aspetc_ratio = self.height/self.width

    def __rich_console__(self, console: Console, options: ConsoleOptions) -> RenderResult:
        w, h = self.fit_in(options.max_width, options.max_height)
        if self.center:
            yield Align.center(self.render(w, h), vertical="middle")
        else:
            yield self.render(w, h)

    def fit_in(self, width: int, height: int) -> (int, int):
        if self.stretch:
            return (width, height)
        outheight = int(self.aspetc_ratio * width * char_pixel_ratio)
        if outheight <= height:
            return width, -1
        return -1, height

    def render(self, width: int = -1, height: int = -1, reverse: bool = False, color: bool = True) -> Text:
        if width < 0 and height < 0:
            raise AttributeError("at least one of width and height must have a valid value")
        if width < 0 or height < 0:
            if width < 0:
                width = int(height / self.aspetc_ratio / char_pixel_ratio)
            else:
                height = int(self.aspetc_ratio * width * char_pixel_ratio)

        img = self.img.resize((width, height))
        if reverse:
            img = PIL.ImageOps.invert(img)

        pixels = Text()
        cp = 0
        for pixel in img.getdata():
            val = (pixel[0] + pixel[1] + pixel[2]) * pixel[3] / 255
            p = chars[int(val//precision)]
            if color:
                pixels.append(p, style=f"rgb({pixel[0]},{pixel[1]},{pixel[2]})")
            else:
                pixels.append(p)
            cp += 1
            if cp >= width:
                pixels.append("\n")
                cp = 0

        return pixels

